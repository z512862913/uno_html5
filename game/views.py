# -*- coding: utf-8 -*-
from models import *
from django.shortcuts import render_to_response
from django.http import JsonResponse
from dwebsocket import require_websocket
import json
from django.contrib.auth.models import User
from django.contrib.auth import login as sys_login, authenticate

# Create your views here.


def index(request):
    return render_to_response('index.html', {})


def login(request):
    if request.method == 'GET':
        if request.user.is_authenticated():
            player = request.user.player
            result = {
                'success': True,
                'data': {
                    'is_login': True,
                    'name': player.name,
                    'avatar': player.avatar,
                }
            }
            return JsonResponse(result, status=200)
        else:
            result = {
                'success': True,
                'message': 'need login'
            }
            return JsonResponse(result, status=401)
    elif request.method == 'POST':
        username = request.POST.get('name')
        avatar = request.POST.get('avatar')
        pwd = '123456'
        for player in Player.objects.all():
            if username == player.name:
                result = {
                    'success': False,
                    'message': 'this username has been chosen'
                }
                return JsonResponse(result, status=403)
        new_user = User()
        new_user.username = username
        new_user.set_password(pwd)
        new_user.save()
        new_player = Player(user=new_user, name=username, avatar=avatar)
        new_player.save()
        user = authenticate(username=username, password=pwd)
        sys_login(request, user)
        result = {'success': True}
        return JsonResponse(result, status=200)


all_websocket_set = {}


def update_game_centre(current_game):
    result = {
        'type': 'room_update',
        'data': {
            'id': current_game.id,
            'name': current_game.name,
            'size': len(json.loads(current_game.order_list)),
            'admin': current_game.admin,
            'users': json.loads(current_game.order_list),
            'is_gaming': current_game.is_playing,
        }
    }

    all_player = Player.objects.all()
    for player in all_player:
        if not player.game_set.all():
            if player.name in all_websocket_set:
                single_websocket = all_websocket_set[player.name]
                single_websocket.send(json.dumps(result))

game_room = Game(name=u'默认游戏房间')
game_room.save()


@require_websocket
def game(request):
    websocket = request.websocket
    init_package = json.loads(websocket.wait())
    player_name = init_package['data']
    player = Player.objects.get(name=player_name)
    all_websocket_set[player_name] = websocket
    game_room.add_person(player)
    current_game = player.game_set.all()[0]  # manytomany_field 可能 炸了
    players = current_game.players.all()
    if len(json.dumps(game_room.order_list)) == 4:
        current_game.is_playing = True
        current_game.start_game()
        current_game.save()
        result = {
            'type': 'game_start',
            'data': {
                'card': [],
                'order': [],
            },
        }
        result['data']['order'] = json.loads(current_game.order_list)
        for single_player in players:
            result['data']['card'] = []
            for card in single_player.card.all():
                card_description = {'number': card.number, 'color': card.color}
                result['data']['card'].append(card_description)
            single_websocket = all_websocket_set[single_player.name]
            single_websocket.send(json.dumps(result))

    result = {
        'type': 'room_member',
        'data': []
    }
    this_player_description = {
        'type': 'room_member_update',
        'data': {
            'name': player_name,
            'avatar': player.avatar,
        }
    }
    for single_player in players:
        single_player_description = {
            'name': single_player.name,
            'avatar': single_player.avatar
        }
        result['data'].append(single_player_description)
        if single_player.name != player_name:
            all_websocket_set[single_player.name].send(json.dumps(this_player_description))
    websocket.send(json.dumps(result))


    '''
    result = {
        'type': 'room_list',
        'data': []
    }
    all_game = Game.objects.all()
    if all_game:
        for game in all_game:
            game_description = {
                'id': game.id,
                'name': game.name,
                'size': len(json.loads(game.order_list)),
                'is_gaming': game.is_playing,
                'admin': game.admin,
                'users': json.loads(game.order_list)
            }
            game['data'].append(game_description)
    websocket.send(json.dumps(result))
    '''
    for message in request.websocket:
        package = json.loads(message)

        if package['type'] == 'play_a_hand':
            current_game = player.game_set.all()[0]
            players = current_game.players.all()
            number = package['data']['number']
            color = package['data']['color']
            card = player.get_a_card(number=number, color=color)
            is_over = player.play_a_hand(card=card)
            result = {
                'type': 'play_a_hand',
                'user': player_name,
                'data': {
                    'number': number,
                    'color': color,
                },
                'is_over': is_over,
            }
            for single_player in players:
                if single_player.name != player.name:
                    single_websocket = all_websocket_set[single_player.name]
                    single_websocket.send(json.dumps(result))
            if is_over:
                pass
                # update_game_centre(current_game)

        elif package['type'] == 'draw_card':
            current_game = player.game_set.all()[0]
            players = current_game.players.all()
            number, color = current_game.distribute_card(player)
            result = {
                'type': 'draw_card',
                'data': {
                    'number': number,
                    'color': color,
                }
            }
            result_for_others = {
                'type': 'draw_card',
                'data': player.name,
            }
            for single_player in players:
                if single_player.name != player.name:
                    single_websocket = all_websocket_set[single_player.name]
                    single_websocket.send(json.dumps(result_for_others))
                else:
                    websocket.send(json.dumps(result))

        elif package['type'] == 'game_start':
            current_game = player.game_set.all()[0]
            players = current_game.players.all()
            current_game.is_playing = True
            current_game.start_game()
            current_game.save()
            result = {
                'type': 'game_start',
                'data': {
                    'card': [
                    ],
                    'order': [
                    ],
                },
            }
            result['data']['order'] = json.loads(current_game.order_list)
            for single_player in players:
                result['data']['card'] = []
                for card in single_player.card.all():
                    card_description = {'number': card.number, 'color': card.color}
                    result['data']['card'].append(card_description)
                single_websocket = all_websocket_set[single_player.name]
                single_websocket.send(json.dumps(result))
            # update_game_centre(current_game)

        if package['type'] == 'room_new':
            room_name = package['data']
            current_game = Game(room_name, player)
            current_game.save()
            result = {
                'type': 'room_new',
                'data': {
                    'id': current_game.id,
                    'name': room_name,
                    'size': 1,
                    'is_gaming': False,
                    'admin': player.name,
                    'users': [
                        player.name,
                    ],
                }
            }
            websocket.send(json.dumps(result))
            # update_game_centre(current_game)

        if package['type'] == 'room_join':
            current_game = Game.objects.get(id=package['data'])
            current_game.add(player)
            players = current_game.players.all()
            result = {
                'type': 'room_member',
                'data': current_game.order_list,
            }
            for single_player in players:
                single_websocket = all_websocket_set[single_player.name]
                single_websocket.send(json.dumps(result))
            # update_game_centre(current_game)

        if package['type'] == 'room_exit':
            current_game = player.game_set.all()[0]
            current_game.del_person(player)
            # update_game_centre(current_game)

    if player.game_set.all():
        current_game = player.game_set.all[0]
        current_game.del_person(player)
    all_websocket_set.pop(player_name)


@require_websocket
def test(request):
    websocket = request.websocket
    package = websocket.wait()
    result = {
        'message': 'room-list'
    }
    websocket.send(json.dumps(result))
    result = {
        'message': 'room-list2'
    }
    websocket.send(json.dumps(result))
