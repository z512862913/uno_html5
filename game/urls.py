from django.conf.urls import include, url
import views


urlpatterns = [
    # Examples:
    # url(r'^$', 'uno_backend.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', views.index),
    url(r'^user/login', views.login),
    url(r'^test', views.test),
    url(r'^game', views.game),
]
