# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0004_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='admin',
            field=models.CharField(default=b'[]', max_length=100, verbose_name='\u7ba1\u7406\u5458\u59d3\u540d'),
        ),
    ]
