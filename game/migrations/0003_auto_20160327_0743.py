# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0002_auto_20160327_0652'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='name',
            field=models.CharField(max_length=100, verbose_name='\u7528\u6237\u59d3\u540d', db_index=True),
        ),
        migrations.AlterField(
            model_name='player',
            name='point',
            field=models.IntegerField(default=0, verbose_name='\u8d62\u5f97\u7684\u5206\u6570'),
        ),
    ]
