# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Card',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.IntegerField(verbose_name='\u5361\u724c\u6570\u5b57')),
                ('color', models.CharField(max_length=10, verbose_name='\u5361\u724c\u989c\u8272')),
            ],
        ),
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u6b64\u573a\u6e38\u620f\u7684\u540d\u79f0')),
                ('is_playing', models.BooleanField(default=False, verbose_name='\u662f\u5426\u5728\u6e38\u620f\u4e2d')),
                ('order_list', models.CharField(default=b'', max_length=1000, verbose_name='\u6240\u6709\u6e38\u620f\u73a9\u5bb6\u540d\u5b57\u7684\u6709\u5e8f\u5217\u8868')),
                ('admin', models.CharField(default=b'', max_length=100, verbose_name='\u7ba1\u7406\u5458\u59d3\u540d')),
                ('card', models.ManyToManyField(default=None, to='game.Card', verbose_name='\u6b64\u573a\u6e38\u620f\u7684\u5361\u5806')),
            ],
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u7528\u6237\u59d3\u540d')),
                ('point', models.IntegerField(default=None, verbose_name='\u8d62\u5f97\u7684\u5206\u6570')),
                ('is_AI', models.BooleanField(default=False, verbose_name='\u662f\u5426\u4e3aAI')),
                ('avatar', models.CharField(max_length=100, verbose_name='\u5934\u50cf\u4ee3\u7801')),
                ('card', models.ManyToManyField(default=None, to='game.Card', verbose_name='\u624b\u4e2d\u7684\u724c')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='game',
            name='player',
            field=models.ManyToManyField(default=None, to='game.Player', verbose_name='\u6b64\u573a\u6e38\u620f\u7684\u73a9\u5bb6'),
        ),
    ]
