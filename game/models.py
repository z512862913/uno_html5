# -*- coding: utf-8 -*-
from django.db import models
import random
from django.contrib.auth.models import User
import json

# Create your models here.


class Card(models.Model):
    number = models.IntegerField(verbose_name=u"卡牌数字")  # 10,11,12,13,14 分别为跳过，翻转，+2,变色，+4
    color = models.CharField(verbose_name=u"卡牌颜色", max_length=10)  # 有red，yellow， blue，green， black


class Player(models.Model):
    user = models.OneToOneField(User)
    name = models.CharField(verbose_name=u"用户姓名", max_length=100, db_index=True)  # 用户1用户2
    point = models.IntegerField(verbose_name=u"赢得的分数", default=0)
    card = models.ManyToManyField(Card, verbose_name=u"手中的牌", default=None)
    is_AI = models.BooleanField(verbose_name=u'是否为AI', default=False)
    avatar = models.CharField(verbose_name=u'头像代码', max_length=100)

    def get_all_legal_play(self):
        """
        :return: 所有可以出的牌的列表, 好像暂时用不到
        """
        pass

    def get_a_card(self, number, color):
        card = self.card.filter(number=number, color=color)[0]
        return card

    def play_a_hand(self, card):
        current_game = self.Game_set.all()[0]
        current_game.card.add(card)
        self.card.remove(card)
        if self.card.all():
            return False
        current_game.is_playing = False
        return True  # True代表已经出完牌了

    def ai_play_a_hand(self):
        """
        :return: ai出的牌的牌面和颜色
        """
        card = self.get_all_legal_play()[0]
        self.play_a_hand(card)
        return card.number, card.color


class Game(models.Model):
    name = models.CharField(verbose_name=u"此场游戏的名称", max_length=100)
    players = models.ManyToManyField(Player, verbose_name=u"此场游戏的玩家", default=None)
    card = models.ManyToManyField(Card, verbose_name=u"此场游戏的卡堆", default=None)
    is_playing = models.BooleanField(verbose_name=u'是否在游戏中', default=False)
    order_list = models.CharField(verbose_name=u'所有游戏玩家名字的有序列表', max_length=1000, default='[]')
    admin = models.CharField(verbose_name=u'管理员姓名', max_length=100, default='')

    def init_deck(self):
        """
        初始化卡堆
        :return:
        """
        for i in ['red', 'yellow', 'blue', 'green']:
            new_card = Card(number=0, color=i)
            self.card.add(new_card)
        for i in range(1, 13):
            for rgb in ['red', 'yellow', 'blue', 'green']:
                new_card1 = Card(number=i, color=rgb)
                new_card2 = Card(number=i, color=rgb)
                self.card.add(new_card1)
                self.card.add(new_card2)
        for i in range(13, 15):
            new_card = Card(number=i, color='black')
            self.card.add(new_card)

    def add_person(self, new_player):
        self.players.add(new_player)
        order_list = json.loads(self.order_list)
        order_list.append(new_player.name)
        self.order_list = json.dumps(order_list)
        self.save()
        pass

    def del_person(self, person):
        self.players.remove(person)
        order_list = json.loads(self.order_list)
        order_list.pop(person.name)
        self.order_list = json.dumps(order_list)
        self.save()
        pass

    def add_ai(self):
        # todo: write
        sentinel = False
        new_ai_name = ''
        for i in range(100):
            new_ai_name = "AI " + str(i)
            for player in self.players.all():
                if player.name != new_ai_name:
                    sentinel = True
            if sentinel:
                break

    def del_ai(self):
        # todo: rewrite
        pass

    def distribute_card(self, player):
        cards_to_distribute = self.card.all()
        card_to_distribute = cards_to_distribute[random.randint(0, len(cards_to_distribute)-1)]
        self.card.remove(card_to_distribute)
        player.card.add(card_to_distribute)
        player.save()
        return card_to_distribute.number, card_to_distribute.color

    def init_distribute_card(self):
        for player in self.players.all():
            for i in range(7):
                self.distribute_card(player)

    def start_game(self):
        self.init_deck()
        self.init_distribute_card()
