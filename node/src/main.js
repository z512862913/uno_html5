import Vue from "vue";
import App from "./component/App.vue";

require("./style/common.css");

Vue.component("app", App);

const app = new Vue({
    el: "#app",
    template: "<app></app>",
});
