import os

def dfs(f, path, vendor):
    files = os.listdir(path)
    for file in files:
        if (file != '.') and (file != '..'):
            file_path = os.path.join(path, file)
            if os.path.isdir(file_path):
                dfs(f, file_path, os.path.join(vendor, file))
            else:
                f.write('"%s": "%s",\n' % (file, os.path.join(vendor, file).replace('\\', '/')))

f = open('list.txt', 'w')
dfs(f, './img', '')
f.close()
