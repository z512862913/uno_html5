
import user from "./user";
import room from "./room";
import game from "./game";

export default {
    user,
    room,
    game,
};
