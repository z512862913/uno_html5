
import { Promise } from "es6-promise";
import api from "../api";

let pending = true;
let isLogin = false;
let name = "";
let avatar = "";

function getLoginStatus() {
    return new Promise((resolve, reject) => {
        if (!pending) {
            resolve({ isLogin });
        }
        else {
            api.getLoginStatus().then((data) => {
                if (typeof data.is_login === "boolean") {
                    pending = false;
                    isLogin = true;
                    name = data.name;
                    avatar = data.avatar;
                    resolve({ isLogin });
                }
                else {
                    reject();
                }
            });
        }
    });
}

function login(user) {
    return api.login(user).then(() => {
        name = user.name;
        avatar = user.avatar;
    });
}

function getInfo() {
    return { isLogin, name, avatar };
}

function notifyServer() {
    api.socket.send({
        type: "user",
        data: name,
    });
}

export default {
    login,
    getLoginStatus,
    getInfo,
    notifyServer,
}
