

import api from "../api";
import user from "./user";

const socket = api.socket;

let name = "asdds";
let card = [ { number: 0, color: "red" },{ number: 0, color: "red" },{ number: 0, color: "red" },{ number: 0, color: "red" } ];
let remain = [ 7, 7, 7, 7 ];
let order = ["sadsads","sadsads","sadsads","sadsads"];
// let first = -1;
let first = 0;
let avatar = ["/static/img/hero/Fury.png","/static/img/hero/Loki.png","/static/img/hero/Hulk.png","/static/img/hero/Fury.png"];

let subscribes = [];

socket.subscribe("room-member", (data) => {
    name = user.getInfo().name;
    card = (data.card || []).slice();
    order = (data.order || []).slice();
    avatar = (data.avatar || []).slice();
    first = 0;
    for (let i = 0; i < order.length; i++) {
        if (order[i] === name) {
            first = order.length - i;
            order = order.slice(i).concat(order.slice(0, i));
            break;
        }
    }

    let fn = null;
    while ((fn = subscribes.pop())) {
        if (fn instanceof Function) {
            fn({ first, card, order, avatar });
        }
    }
});

socket.subscribe("room-member_roomupdate", (data) => {
    order.push(data.name);
    order.push(data.avatar);
});

function getInfo(fn) {
    if (first === -1) {
        subscribes.push(fn);
    }
    else if (fn instanceof Function) {
        fn({ first, card, order, avatar });
    }
};

function subscribe(conf = {}) {
    for (let type in conf) {
        socket.subscribe(type, conf[type]);
    }
};

export default {
    getInfo,
    subscribe,
};
