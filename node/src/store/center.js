
import api from "../api";

const socket = api.socket;
let list = [];

socket.subscribe("room-list", (data) => {
    list = (data || []).slice();
    console.log(list);
});

socket.subscribe("room-new", (data) => {
    list.push(data || {});
});

socket.subscribe("room-update", (data) => {
    for (var i = 0; i < list.length; i++) {
        if (list[i].id === data.id) {
            for (var key in data) {
                list[i][key] = data;
            }
            return;
        }
    }
});

function getRoomList() {
    return list;
}

function createRoom() {
    return socket.send("room-new");
}

export default {
    getRoomList,
    createRoom,
};
