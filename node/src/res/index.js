
import api from "../api";
import { Promise } from "es6-promise";

const HASH_LEN = 6;

const publicPath = "/static/img/";
const files = {
    pre: {
        "shied": "shield.png",
    },
    load: {
        "Gay.png": "hero/Gay.png",
        "Fury.png": "hero/Fury.png",
        "Hulk.png": "hero/Hulk.png",
        "Hawkeye.png": "hero/Hawkeye.png",
        "Iron.png": "hero/Iron.png",
        "loki.png": "hero/Loki.png",
        "American.png": "hero/American.png",
        "Widow.png": "hero/Widow.png",
        "Raytheon.png": "hero/Raytheon.png",
        "DRAW4.png": "card/DRAW4.png",
        "blue_2.png": "card/blue_2.png",
        "red_4.png": "card/red_4.png",
        "red_6.png": "card/red_6.png",
        "green_skip.png": "card/green_skip.png",
        "blue_0.png": "card/blue_0.png",
        "red_0.png": "card/red_0.png",
        "green_9.png": "card/green_9.png",
        "yellow_+2.png": "card/yellow_+2.png",
        "blue_reverse.png": "card/blue_reverse.png",
        "red_8.png": "card/red_8.png",
        "yellow_1.png": "card/yellow_1.png",
        "red_7.png": "card/red_7.png",
        "red_2.png": "card/red_2.png",
        "green_3.png": "card/green_3.png",
        "red_+2.png": "card/red_+2.png",
        "green_reverse.png": "card/green_reverse.png",
        "red_reverse.png": "card/red_reverse.png",
        "yellow_5.png": "card/yellow_5.png",
        "blue_+2.png": "card/blue_+2.png",
        "yellow_3.png": "card/yellow_3.png",
        "Wild.png": "card/Wild.png",
        "red_9.png": "card/red_9.png",
        "blue_skip.png": "card/blue_skip.png",
        "red_1.png": "card/red_1.png",
        "blue_6.png": "card/blue_6.png",
        "red_skip.png": "card/red_skip.png",
        "green_7.png": "card/green_7.png",
        "green_1.png": "card/green_1.png",
        "yellow_6.png": "card/yellow_6.png",
        "green_6.png": "card/green_6.png",
        "green_2.png": "card/green_2.png",
        "green_+2.png": "card/green_+2.png",
        "red_3.png": "card/red_3.png",
        "yellow_8.png": "card/yellow_8.png",
        "green_8.png": "card/green_8.png",
        "yellow_2.png": "card/yellow_2.png",
        "green_4.png": "card/green_4.png",
        "yellow_4.png": "card/yellow_4.png",
        "green_0.png": "card/green_0.png",
        "yellow_reverse.png": "card/yellow_reverse.png",
        "yellow_0.png": "card/yellow_0.png",
        "blue_9.png": "card/blue_9.png",
        "yellow_7.png": "card/yellow_7.png",
        "blue_3.png": "card/blue_3.png",
        "red_5.png": "card/red_5.png",
        "blue_5.png": "card/blue_5.png",
        "yellow_9.png": "card/yellow_9.png",
        "yellow_skip.png": "card/yellow_skip.png",
        "blue_7.png": "card/blue_7.png",
        "blue_1.png": "card/blue_1.png",
        "blue_4.png": "card/blue_4.png",
        "green_5.png": "card/green_5.png",
        "blue_8.png": "card/blue_8.png",
    },
};

let isPre = false;
let isLoad = false;

function pre() {
    if (isPre) {
        return;
    }

    const list = Object.keys(files.pre);
    return Promise.all(list.map((name) => {
        let path = publicPath + files.pre[name] + "?" + parseInt(Math.random() * Math.pow(10, HASH_LEN));
        files.pre[name] = path;
        return api.get(path);
    }));
}

function load(fn) {
    if (isLoad) {
        return;
    }

    if (!(fn instanceof Function)) {
        fn = function () {};
    }

    const list = Object.keys(files.load);
    list.forEach((name) => {
        let path = publicPath + files.load[name] + "?" + parseInt(Math.random() * Math.pow(10, HASH_LEN));
        files.load[name] = path;
        console.log(files.load[name]);
        api.get(path).then((data) => {
            fn(name);
        });
    });
    return list.length;
}

function get(name) {
    console.log(name);
    return files.load[name];
}

export default {
    pre,
    load,
    get,
};
