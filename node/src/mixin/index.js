
var wrapper = {
    methods: {
        resize() {
            const outerHeight = this.$el.offsetHeight;
            const innerHeight = this.$els.wrapper.offsetHeight;
            this.$el.paddingTop = (outerHeight - innerHeight) / 2 + "px";
        },
    },
    ready() {
        this.resize();
        window.addEventListener("resize", () => {
            this.resize();
        });
    },
}

export default {
    wrapper,
};
