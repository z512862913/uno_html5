# -*- coding: utf-8 -*-
import os

files = os.listdir('.')
for file in files:
    if (file != '.') and (file != '..'):
        name = file.replace(u'红色卡牌'.encode('gbk'), 'red_')
        name = name.replace(u'黄色卡牌'.encode('gbk'), 'yellow_')
        name = name.replace(u'蓝色卡牌'.encode('gbk'), 'blue_')
        name = name.replace(u'绿色卡牌'.encode('gbk'), 'green_')
        if file != name:
            os.rename(file, name)