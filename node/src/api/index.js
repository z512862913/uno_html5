import Vue from "vue";
import VueResource from "vue-resource";

import { Promise } from "es6-promise";
import socket from "./socket";

Vue.use(VueResource);

Vue.http.options.emulateJSON = true;

function login(user) {
    return Vue.http({
        url: "/user/login/",
        data: user,
        method: "POST",
    }).then((response) => {
        return Promise.resolve(response.data.data);
    });
}

function getLoginStatus() {
    return Vue.http("/user/login/").then((response) => {
        return Promise.resolve(response.data.data);
    });
}

function get(path) {
    return Vue.http(path).then((response) => {
        return Promise.resolve(response.data.data);
    });
}

export default {
    get,
    login,
    getLoginStatus,
    socket,
};
