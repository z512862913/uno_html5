
import { Promise } from "es6-promise";

const HOST = "0.0.0.0";
const PORT = "2333";
const URL = "/game"
const SERVER = "ws://" + HOST + ":" + PORT + URL;

let ws = null;
let subscribes = {};

function onOpen() {
    window.console.log("WebSocket open...");
}

function onClose() {
    window.console.log("WebSocket close...");
}

function onMessage(msg) {
    window.console.log("WebSocket message: " + msg.data);

    if (typeof msg.data === "string") {
        let data;
        try {
            data = JSON.parse(msg.data);
        }
        catch(e) {
            data = { message: msg.data, data: null };
        }
        publish(data);
    }
}

function onError() {
    window.console.log("WebSocket some errors occured...");
}

function onTimeout() {
    window.console.log("WebSocket timeout...");
}

function waitForConnection(fn, interval = 1000) {
    if (fn instanceof Function) {
        let instance = window.setInterval(() => {
            if (ws.readyState === 1) {
                window.clearInterval(instance);
                fn();
            }
        }, interval);
    }
}

function connect() {
    try {
        ws = new WebSocket(SERVER);
        ws.onopen = onOpen;
        ws.onclone = onClose;
        ws.onmessage = onMessage;
        ws.onerror = onError;
        ws.ontimeout = onTimeout;
    }
    catch(e) {
        window.console.log(e);
    }
}

function send(msg) {
    return new Promise((resolve, reject) => {
        if (!ws) {
            connect();
        }
        waitForConnection(() => {
            window.console.log(msg);
            ws.send(JSON.stringify(msg));
            resolve();
        }, 1000);
    });
}

function subscribe(type, fn) {
    if (!ws) {
        connect();
    }
    if (!subscribes[type]) {
        subscribes[type] = [];
    }
    if (fn instanceof Function) {
        subscribes[type].push(fn);
    }
}

function publish({ message, data }) {
    if (typeof message === "string") {
        (subscribes[message] || []).forEach((fn) => {
            fn(data);
        });
    }
}

export default {
    connect,
    send,
    subscribe,
};
