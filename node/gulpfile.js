const gulp = require("gulp");

// gulp.task("webpack", function(callback) {
//     // run webpack
//     webpack(conf, function(err, stats) {
//     });
// });

gulp.task("image", function () {
    gulp.src("./src/img/**")
        .pipe(gulp.dest("../static/img"))
});

gulp.task("default", function () {
    gulp.watch("./src/img/**", [ "image" ]);
});