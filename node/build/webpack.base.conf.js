var path = require('path')
var autoprefixer = require('autoprefixer')
var ExtractTextPlugin = require("extract-text-webpack-plugin")
var cssLoaders = require('./css-loaders')
var projectRoot = path.resolve(__dirname, '../')

module.exports = {
  entry: {
    app: './src/main.js'
  },
  output: {
    path: path.resolve(__dirname, '../../static'),
    publicPath: '/static/',
    filename: 'js/[name].js'
  },
  resolve: {
    extensions: ['', '.js', '.vue'],
    fallback: [path.join(__dirname, '../node_modules')],
    alias: {
      'src': path.resolve(__dirname, '../src')
    }
  },
  resolveLoader: {
    fallback: [path.join(__dirname, '../node_modules')]
  },
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue'
      },
      {
        test: /\.js$/,
        loader: 'babel',
        include: projectRoot,
        exclude: /node_modules/
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.html$/,
        loader: 'vue-html'
      },
      {
        test: /\.css$/,
        loader:  ExtractTextPlugin.extract("style", "css!postcss"),
        // loader: "style!css!postcss",
        },
    //   {
    //     test: /\.(png|jpg|gif|svg)(\?.*)?$/,
    //     loader: 'url',
    //     query: {
    //       limit: 1,
    //       name: 'img/[name].[ext]?[hash:7]'
    //     }
    //   }
    ]
  },
  vue: {
    loaders: cssLoaders()
  },
  postcss: [ autoprefixer({ browsers: ['last 2 versions'] }) ],
  plugins: [
      new ExtractTextPlugin('css/[name].[contenthash].css'),
  ]
}
