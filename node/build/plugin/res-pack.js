var fs = require("fs");

var output = "";
var vendors = [];

function ResLoader(options) {
    output = options.output;
    vendors = options.vendors;
}

ResLoader.prototype.apply = function (compiler) {
    compiler.plugin("done", function (stats) {
        var paths = [];
        for (path in stats.compilation.assets) {
            for (vendor in vendors) {
                if (path.indexOf(vendor) === 0) {
                    paths.push(path);
                }
            }
        }

        var string = "";
    });
}

module.exports = ResLoader;